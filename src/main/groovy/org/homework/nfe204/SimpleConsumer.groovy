package org.homework.nfe204

import org.homework.nfe204.from.FromKafka

// -k 172.18.0.3:9092,172.18.0.4:9092 -t test-consumer-topic -n william
class SimpleConsumer {

    private String consumerName

    private String groupName

    private void init() {
    }

    String getGroup() {
        return groupName
    }

    String getName() {
        return consumerName
    }

    static SimpleConsumer build() {
        SimpleConsumer consumer = new SimpleConsumer()
        consumer.init()
        return consumer
    }

    static void main(String[] args) {
        def cli = new CliBuilder(usage: 'java -jar predictor-1.0-SNAPSHOT-shaded.jar -k 172.18.0.3:9092 -t test-consumer-topic -n william -g angleterre')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            k longOpt: 'kafkaBrokers', required: false, args: 1, argName: 'kafkaBrokers', 'Liste des urls des noeuds Kafka'
            t longOpt: 'topic', required: true, args: 1, argName: 'topic', 'Nom du topic à consommer'
            g longOpt: 'group', required: true, args: 1, argName: 'group', 'Nom du groupe'
            n longOpt: 'name', required: true, args: 1, argName: 'name', 'Nom du process'
        }
        def options = cli.parse(args)
        if (!options) {
            return
        }
        if (options.h) {
            cli.usage()
        }

        if (options.t) {
            Properties props = new Properties()
            props.put("bootstrap.servers", options.k)
            props.put("group.id", options.g)
            props.put("client.id", options.n)
            props.put("key.deserializer",
                    "org.apache.kafka.common.serialization.StringDeserializer")
            props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
            SimpleConsumer simpleConsumer = new SimpleConsumer()
            simpleConsumer.groupName = options.g
            simpleConsumer.consumerName = options.n
            FromKafka.builder().properties(props).topic(options.t).build().start(simpleConsumer)
        }
    }
}
