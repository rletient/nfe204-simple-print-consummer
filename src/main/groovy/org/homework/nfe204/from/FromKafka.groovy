package org.homework.nfe204.from

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.homework.nfe204.SimpleConsumer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class FromKafka {

    private static Logger LOGGER = LoggerFactory.getLogger(FromKafka.class)

    private KafkaConsumer<String, String> consumer
    private String topic

    void start(SimpleConsumer consumer) {
        this.consumer.subscribe(Arrays.asList(topic))
        try {
            while (true) {
                ConsumerRecords<String, String> records = this.consumer.poll(100)
                for (ConsumerRecord<String, String> record : records) {
                    LOGGER.info("${consumer.getGroup()} / ${consumer.getName()} > Partition : ${record.partition()} - Offset : ${record.offset()} - Key : ${record.key()} - Value : ${record.value()}")
                }
            }
        } finally {
            this.consumer.close()
        }
    }

    static Builder builder() {
        return new Builder()
    }

    static final class Builder {
        private Properties properties

        private String topic

        Builder topic(String topic) {
            this.topic = topic
            return this
        }

        Builder properties(Properties properties) {
            this.properties = properties
            return this
        }

        FromKafka build() {
            KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties)
            FromKafka fromKafka = new FromKafka()
            fromKafka.consumer = consumer
            fromKafka.topic = topic
            return fromKafka
        }
    }
}
